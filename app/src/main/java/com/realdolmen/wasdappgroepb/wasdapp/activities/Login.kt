package com.realdolmen.wasdappgroepb.wasdapp.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.widget.EditText
import android.widget.Toast
import com.realdolmen.wasdappgroepb.wasdapp.R
import com.realdolmen.wasdappgroepb.wasdapp.adapters.RetroFitFactory.Companion.retrofitU
import com.realdolmen.wasdappgroepb.wasdapp.dataclasses.User
import com.realdolmen.wasdappgroepb.wasdapp.dataclasses.UserBuilder
import com.realdolmen.wasdappgroepb.wasdapp.repo.UserRepo
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Login : AppCompatActivity() {
    private lateinit var userRepo: UserRepo
    private lateinit var userBuilder: UserBuilder
    private var passwordLogin: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initRepo()
        loginBtn.setOnClickListener { loginUser() }
        registerBtn.setOnClickListener{
            val intent = Intent(this, AddUser::class.java)
            intent.putExtra("isAddUser",false)
            startActivity(intent)
        }
    }
    private fun initRepo() {
        userRepo = retrofitU.create(UserRepo::class.java)
    }
    private fun loginUser() {
        val intent = Intent(this, Homepage::class.java)
        intent.action = Intent.ACTION_SEND
        passwordLogin = findViewById(R.id.et_passwordLogin)
        userBuilder = UserBuilder()
        var userActual = userRepo.checkUserLogin(
            userBuilder.setUsername(et_usernameLogin.text.trim().toString())
                .setPassword(passwordLogin!!.text.trim().toString()).build()
        )

        userActual?.enqueue(object : Callback<User> {
            override fun onFailure(call: Call<User>, t: Throwable) {
                println(t.message)
            }

            override fun onResponse(call: Call<User>, response: Response<User>) {
                if (et_usernameLogin.text.trim().toString().isNullOrEmpty()) {
                    et_usernameLogin.error = "Username can't be empty"
                } else if (et_passwordLogin.text.trim().toString().isNullOrEmpty()) {
                    et_passwordLogin.error = "password can't be empty"
                } else {
                    if (response.body()!!.userName.equals("false")) {

                        val toast = Toast.makeText(
                            applicationContext,
                            "Incorrect username or password", Toast.LENGTH_SHORT
                        )
                        toast.setGravity(Gravity.CENTER, 0, 0)
                        toast.show()

                    } else {
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        intent.putExtra("isAdmin", response.body()!!.admin)
                        startActivity(intent)
                        val toast = Toast.makeText(
                            applicationContext,
                            " Welcome ${response.body()!!.userName}.", Toast.LENGTH_SHORT
                        )
                        toast.setGravity(Gravity.CENTER, 0, 0)
                        toast.show()

                    }
                }
            }

        })
    }
    private var back_pressed: Long = 0
    private var toast: Toast? = null
    override fun onBackPressed() {


        if (back_pressed + 2000 > System.currentTimeMillis()) {

            // need to cancel the toast here
            toast!!.cancel()

            // code for exit
            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_HOME)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)

        } else {
            // ask user to press back button one more time to close app
            toast = Toast.makeText(baseContext, "Press BACK again to exit", Toast.LENGTH_SHORT)
            toast!!.show()
        }
        back_pressed = System.currentTimeMillis()
    }
}
