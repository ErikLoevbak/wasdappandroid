package com.realdolmen.wasdappgroepb.wasdapp.adapters

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import cc.cloudist.acplibrary.ACProgressConstant
import cc.cloudist.acplibrary.ACProgressFlower
import com.google.gson.GsonBuilder
import com.realdolmen.wasdappgroepb.wasdapp.R
import com.realdolmen.wasdappgroepb.wasdapp.activities.Homepage
import com.realdolmen.wasdappgroepb.wasdapp.activities.Insert
import com.realdolmen.wasdappgroepb.wasdapp.activities.WasdappDetailPage
import com.realdolmen.wasdappgroepb.wasdapp.dataclasses.WasdappEntry
import com.realdolmen.wasdappgroepb.wasdapp.repo.WasdappRepo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class WasdappAdapter(
    private val list: ArrayList<WasdappEntry>,
    private val context: Context,
    private val Activity: Homepage

) :

    RecyclerView.Adapter<WasdappAdapter.ViewHolder>() {
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl("http://wp1697:8080/wasdappentry/").build()


        fun bindItem(wasdappEntry: WasdappEntry) {
            checkadmin(itemView)
            var name: TextView = itemView.findViewById(R.id.lv_name)
            var location: TextView = itemView.findViewById(R.id.lv_location)
            var id: TextView = itemView.findViewById(R.id.lv_id)
            val delete: TextView = itemView.findViewById(R.id.delete_Product)
            val edit: TextView = itemView.findViewById(R.id.edit)

            name.text = wasdappEntry.name
            location.text = wasdappEntry.city
            id.text = wasdappEntry.id.toString()

            itemView.setOnClickListener {

                val dialog = ACProgressFlower.Builder(context)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .text("Loading...")
                    .fadeColor(Color.DKGRAY).build()
                dialog.show()
                Executors.newSingleThreadScheduledExecutor().schedule({
                    dialog.dismiss()
                }, 2, TimeUnit.SECONDS)


                val intent = Intent(context, WasdappDetailPage::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.putExtra("idEntry", wasdappEntry.id)
                context.startActivity(intent)
            }

            edit.setOnClickListener {
                val intent = Intent(context, Insert::class.java)
                intent.putExtra("idEntry", wasdappEntry.id)
                context.startActivity(intent)
            }


            delete.setOnClickListener {
                val builder = AlertDialog.Builder(Activity)

                builder.setTitle("Delete")

                builder.setMessage("Are you sure you want to delete the selected item with name: " + name.text)

                builder.setPositiveButton("Confirm") { _, _ ->

                    Toast.makeText(Activity, "Delete has been confirmed, executing...", Toast.LENGTH_SHORT).show()
                    var wasDappEntryService = retrofit.create(WasdappRepo::class.java)
                    var wassdapActual = wasDappEntryService.deleteById(wasdappEntry.id!!.toLong())
                    wassdapActual.enqueue(object : Callback<Boolean> {
                        override fun onResponse(call: Call<Boolean>, response: Response<Boolean>) {
                            var intent = Intent(context, Homepage::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                            context.startActivity(intent)

                        }

                        override fun onFailure(call: Call<Boolean>, t: Throwable) {
                            println(t.message)

                        }
                    })
                }

                builder.setNeutralButton("Cancel") { dialogInterface: DialogInterface, _: Int ->
                    Toast.makeText(Activity, "Delete has been canceled, good bye.", Toast.LENGTH_SHORT).show()
                    dialogInterface.dismiss()
                }

                val dialog: AlertDialog = builder.create()

                dialog.show()
            }

        }

        val isAdmin = Intent(context, Homepage::class.java)
        var isAdminOrNot = isAdmin.getBooleanExtra("isAdmin", true)

        private fun checkadmin(view: View) {
            if (isAdminOrNot) {

                var deleteProduct: Button = itemView.findViewById(R.id.delete_Product)
                var editProduct: Button = itemView.findViewById(R.id.edit)

                deleteProduct.visibility = View.VISIBLE
                editProduct.visibility = View.VISIBLE


            } else {
                var deleteProduct: Button = itemView.findViewById(R.id.delete_Product)
                var editProduct: Button = itemView.findViewById(R.id.edit)

                deleteProduct.visibility = View.INVISIBLE
                editProduct.visibility = View.INVISIBLE
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.cardview_main, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(list[position])
    }


}