package com.realdolmen.wasdappgroepb.wasdapp.activities

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.location.Location
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import com.realdolmen.wasdappgroepb.wasdapp.R
import com.realdolmen.wasdappgroepb.wasdapp.adapters.RetroFitFactory.Companion.retrofit
import com.realdolmen.wasdappgroepb.wasdapp.dataclasses.WasdappEntry
import com.realdolmen.wasdappgroepb.wasdapp.repo.WasdappRepo
import kotlinx.android.synthetic.main.activity_detail_product.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class WasdappDetailPage : AppCompatActivity() {
    private var wasdapDetailProduct: WasdappEntry? = null
    private var bitmap: Bitmap? = null
    private var idResult: TextView? = null
    private var img: ImageView? = null
    private var distance: Float? = null
    private lateinit var startPoint: LatLng
    private val afstand: Float = 75.0f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_product)
        var fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                1
            )
        }
        facebookshare.setOnClickListener {
            fusedLocationClient.lastLocation
                .addOnSuccessListener(this) { location: Location? ->

                    if (location?.hasAltitude()!!) {
                        var start = Location("Test")
                        start.latitude = startPoint.latitude
                        start.longitude = startPoint.longitude
                        distance = start?.distanceTo(location)
                        if (distance as Float <= afstand) {
                            val sendIntent: Intent = Intent().apply {
                                action = Intent.ACTION_SEND
                                putExtra(
                                    Intent.EXTRA_TEXT,
                                    "Hey, I just scanned " + tv_name.text + " located @ " + tv_city.text
                                )
                                type = "text/plain"
                            }
                            startActivity(sendIntent)
                        } else {
                            Toast.makeText(this, "To far from location, must be in range of 75m", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                }.addOnFailureListener {
                    println(it.message)
                }
        }
        img = findViewById(R.id.imageView)
        idResult = tv_id as TextView

        if (idResult!!.text.toString().trim { it <= ' ' }.isEmpty()) {
            Toast.makeText(this@WasdappDetailPage, "must have a value", Toast.LENGTH_SHORT).show()
        } else {
            try {
                bitmap = TextToImageEncode(intent.getLongExtra("idEntry", 0).toString())
                img!!.setImageBitmap(bitmap)


            } catch (e: WriterException) {
                e.printStackTrace()
            }
        }


        var wasDappEntryService = retrofit.create(WasdappRepo::class.java)
        var id = intent.getLongExtra("idEntry", 0)

        val actualEntry = wasDappEntryService.getOne(id)

        actualEntry.enqueue(object : Callback<WasdappEntry> {
            override fun onResponse(call: Call<WasdappEntry>, response: Response<WasdappEntry>) {
                println(response.body()!!)
                wasdapDetailProduct = (response.body()!!)
                tv_name.text = wasdapDetailProduct!!.name
                tv_website.text = wasdapDetailProduct!!.web
                tv_email.text = wasdapDetailProduct!!.email
                tv_street.text = wasdapDetailProduct!!.street
                tv_addressNr.text = wasdapDetailProduct!!.number
                tv_postcode.text = wasdapDetailProduct!!.postcode
                tv_city.text = wasdapDetailProduct!!.city
                tv_omschrijving.text = wasdapDetailProduct!!.description
                startPoint = LatLng(wasdapDetailProduct!!.lat!!, wasdapDetailProduct!!.lon!!)
            }

            override fun onFailure(call: Call<WasdappEntry>, t: Throwable) {
            }
        })
    }

    @Throws(WriterException::class)
    private fun TextToImageEncode(Value: String): Bitmap? {
        val bitMatrix: BitMatrix
        try {
            bitMatrix = MultiFormatWriter().encode(
                Value,
                BarcodeFormat.QR_CODE,
                QRcodeWidth, QRcodeWidth, null
            )

        } catch (Illegalargumentexception: IllegalArgumentException) {

            return null
        }

        val bitMatrixWidth = bitMatrix.width

        val bitMatrixHeight = bitMatrix.height

        val pixels = IntArray(bitMatrixWidth * bitMatrixHeight)

        for (y in 0 until bitMatrixHeight) {
            val offset = y * bitMatrixWidth

            for (x in 0 until bitMatrixWidth) {

                pixels[offset + x] = if (bitMatrix.get(x, y))
                    resources.getColor(R.color.black)
                else
                    resources.getColor(R.color.white)
            }
        }
        val bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444)

        bitmap.setPixels(pixels, 0, 500, 0, 0, bitMatrixWidth, bitMatrixHeight)
        return bitmap
    }

    companion object {

        const val QRcodeWidth = 500
    }


}
