package com.realdolmen.wasdappgroepb.wasdapp.dataclasses


import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("admin")
    val admin: Boolean?,
    @SerializedName("id")
    val id: Long?,
    @SerializedName("password")
    val password: String?,
    @SerializedName("userName")
    val userName: String?
)