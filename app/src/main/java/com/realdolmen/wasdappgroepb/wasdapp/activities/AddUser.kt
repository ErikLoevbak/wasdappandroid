package com.realdolmen.wasdappgroepb.wasdapp.activities

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.google.zxing.integration.android.IntentIntegrator
import com.google.zxing.integration.android.IntentResult
import com.realdolmen.wasdappgroepb.wasdapp.R
import com.realdolmen.wasdappgroepb.wasdapp.adapters.RetroFitFactory.Companion.retrofitU
import com.realdolmen.wasdappgroepb.wasdapp.dataclasses.UserBuilder
import com.realdolmen.wasdappgroepb.wasdapp.repo.UserRepo
import kotlinx.android.synthetic.main.activity_add_user.*
import kotlinx.android.synthetic.main.app_bar_add_user.*
import kotlinx.android.synthetic.main.content_add_user.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddUser : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    var isAddUser = true
    private var scannedResult: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_add_user)
        super.onCreate(savedInstanceState)
        if (!intent.getBooleanExtra("isAddUser", true)) {
            isAddUser = false
            check_admin.visibility = View.GONE
            adminpic_add_user.visibility = View.GONE
            btn_addUser.text = "Register"
            title = "Register"
        }
        setDrawer()



        btn_addUser.setOnClickListener {
            addUser()
        }

    }

    private fun setDrawer() {
        setSupportActionBar(toolbarAddUser)
        val toggle = ActionBarDrawerToggle(
            this, drawer_layout_add_user, toolbarAddUser,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer_layout_add_user.addDrawerListener(toggle)
        toggle.syncState()
        toggle.isDrawerIndicatorEnabled = isAddUser
        nav_view_add_user.setNavigationItemSelectedListener(this)
    }

    override fun onBackPressed() {
        if (isAddUser) {
            if (drawer_layout_add_user.isDrawerOpen(GravityCompat.START)) {
                drawer_layout_add_user.closeDrawer(GravityCompat.START)
            } else {
                super.onBackPressed()
            }
        } else {
            var intent = Intent(this, Login::class.java)
            startActivity(intent)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.add_user, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        var result: IntentResult? = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)

        if (result != null && resultCode == RESULT_OK) {

            scannedResult = result.contents
            if (scannedResult.toLongOrNull() != null) {
                val intent = Intent(this, WasdappDetailPage::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.putExtra("idEntry", scannedResult.toLong())
                this.startActivity(intent)
            } else {
                val saveAlert = AlertDialog.Builder(this)
                saveAlert.setTitle("Scan Error")
                saveAlert.setMessage("QR code did not contain a number!")
                saveAlert.setPositiveButton("OK") { dialogInterface: DialogInterface, i: Int ->
                    dialogInterface.dismiss()
                }
                saveAlert.show()
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_addUser -> {
                val intent = Intent(this, AddUser::class.java)
                intent.action = Intent.ACTION_SEND
                startActivity(intent)
            }
            R.id.nav_addRecord -> {
                val intent = Intent(this, Insert::class.java)
                startActivity(intent)
            }
            R.id.nav_scanCode -> {
                IntentIntegrator(this).initiateScan()
            }
            R.id.nav_viewRecord -> {
                val intent = Intent(this, Homepage::class.java)
                intent.action = Intent.ACTION_SEND
                startActivity(intent)
            }
            R.id.nav_logOut -> {
                val intent = Intent(this, Login::class.java)
                intent.action = Intent.ACTION_SEND
                startActivity(intent)
            }
        }
        drawer_layout_add_user.closeDrawer(GravityCompat.START)
        return true
    }

    private fun addUser() {
        var repo = initRepo()
        var username = et_username.text.toString()
        var password = et_password.text.toString()
        var passwordConfirm = et_passwordConfirm.text.toString()
        var isAdmin = false
        if (isAddUser) {
            isAdmin = check_admin.isChecked
        }

        var user = UserBuilder().setUsername(username).setPassword(password).setIsAdmin(isAdmin).build()

        if (username.isNotEmpty() && password.isNotEmpty() && passwordConfirm.isNotEmpty()) {
            if (password.length in 6..24) {
                if (passwordConfirm == password) {
                    repo.save(user).enqueue(object : Callback<Boolean> {
                        override fun onFailure(call: Call<Boolean>, t: Throwable) {
                            Toast.makeText(applicationContext, "Could not connect to database.", Toast.LENGTH_LONG)
                                .show()
                        }

                        override fun onResponse(call: Call<Boolean>, response: Response<Boolean>) {
                            if (response.code() == 200) {
                                if (response.body() as Boolean) {

                                    Toast.makeText(
                                        applicationContext,
                                        "User $username has been successfully added to the database.",
                                        Toast.LENGTH_LONG
                                    )
                                        .show()
                                    et_username.setText("")
                                    et_password.setText("")
                                    et_passwordConfirm.setText("")
                                    check_admin.isChecked = false
                                }
                                if (!(response.body() as Boolean)) {
                                    et_username.error = "Username already exists!"
                                }
                            }
                        }

                    })

                } else {
                    et_passwordConfirm.error = "Passwords must match."
                }

            } else {
                et_password.error = "Password must be between 6 and 25 characters."
            }

        } else {
            et_username.error = "Please enter all fields"
        }
    }

    private fun initRepo(): UserRepo {
        return retrofitU.create(UserRepo::class.java)

    }
}