package com.realdolmen.wasdappgroepb.wasdapp.dataclasses


import com.google.gson.annotations.SerializedName

data class WasdappEntry(
    @SerializedName("city")
    val city: String?,
    @SerializedName("description")
    val description: String?,
    @SerializedName("email")
    val email: String?,
    @SerializedName("id")
    val id: Long?,
    @SerializedName("lat")
    val lat: Double?,
    @SerializedName("lon")
    val lon: Double?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("number")
    val number: String?,
    @SerializedName("postcode")
    val postcode: String?,
    @SerializedName("street")
    val street: String?,
    @SerializedName("tel")
    val tel: String?,
    @SerializedName("web")
    val web: String?,
    @SerializedName("wiki")
    val wiki: String?
)
