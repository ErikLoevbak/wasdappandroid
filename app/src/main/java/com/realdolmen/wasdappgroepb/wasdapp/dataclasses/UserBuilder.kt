package com.realdolmen.wasdappgroepb.wasdapp.dataclasses

class UserBuilder {
    private var id: Long? = null
    private var userName: String? = null
    private var password: String? = null
    private var admin: Boolean? = null

    fun setUsername(username: String?): UserBuilder {
        this.userName = username
        return this
    }

    fun setPassword(password: String?): UserBuilder {
        this.password = password
        return this
    }

    fun setIsAdmin(admin: Boolean?): UserBuilder {
        this.admin = admin
        return this
    }


    fun build(): User {
        return User(admin, id, password, userName)
    }
}

