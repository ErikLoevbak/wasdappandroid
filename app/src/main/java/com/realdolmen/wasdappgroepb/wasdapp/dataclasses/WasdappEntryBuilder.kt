package com.realdolmen.wasdappgroepb.wasdapp.dataclasses

class WasdappEntryBuilder {
    private var city: String? = null
    private var description: String? = null
    private var email: String? = null
    private var id: Long? = null
    private var lat: Double? = null
    private var lon: Double? = null
    private var name: String? = null
    private var number: String? = null
    private var postcode: String? = null
    private var street: String? = null
    private var tel: String? = null
    private var web: String? = null
    private var wiki: String? = null

    fun setCity(city: String?): WasdappEntryBuilder {
        this.city = city
        return this
    }

    fun setDescription(description: String?): WasdappEntryBuilder {
        this.description = description
        return this
    }

    fun setEmail(email: String?): WasdappEntryBuilder {
        this.email = email
        return this
    }

    fun setId(id: Long): WasdappEntryBuilder {
        this.id = id
        return this
    }

    fun setLat(lat: Double?): WasdappEntryBuilder {
        this.lat = lat
        return this
    }

    fun setLon(lon: Double?): WasdappEntryBuilder {
        this.lon = lon
        return this
    }

    fun setName(name: String): WasdappEntryBuilder {
        this.name = name
        return this
    }

    fun setNumber(number: String?): WasdappEntryBuilder {
        this.number = number
        return this
    }

    fun setPostcode(postcode: String?): WasdappEntryBuilder {
        this.postcode = postcode
        return this
    }

    fun setStreet(street: String?): WasdappEntryBuilder {
        this.street = street
        return this
    }

    fun setTel(tel: String?): WasdappEntryBuilder {
        this.tel = tel
        return this
    }

    fun setWeb(web: String?): WasdappEntryBuilder {
        this.web = web
        return this
    }

    fun setWiki(wiki: String?): WasdappEntryBuilder {
        this.wiki = wiki
        return this
    }

    fun build(): WasdappEntry {
        return WasdappEntry(city, description, email, id, lat, lon, name, number, postcode, street, tel, web, wiki)
    }


}

