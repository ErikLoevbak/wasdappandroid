package com.realdolmen.wasdappgroepb.wasdapp.repo

import com.realdolmen.wasdappgroepb.wasdapp.dataclasses.User
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface UserRepo {
    @POST("login")
    fun checkUserLogin(@Body user: User): Call<User>

    @POST("register")
    fun save(@Body user: User): Call<Boolean>
}