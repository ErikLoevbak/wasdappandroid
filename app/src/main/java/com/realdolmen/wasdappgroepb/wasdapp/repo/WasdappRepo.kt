package com.realdolmen.wasdappgroepb.wasdapp.repo

import com.realdolmen.wasdappgroepb.wasdapp.dataclasses.WasdappEntry
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*


interface WasdappRepo {
    @GET("get/{id}")
    fun getOne(@Path("id") id: Long): Call<WasdappEntry>

    @GET("search/{searchRequest}")
    fun search(@Path("searchRequest") searchRequest: String): Call<ArrayList<WasdappEntry>>

    @POST("insert")
    fun insert(@Body wasdappEntry: WasdappEntry): Call<Boolean>

    @GET("get")
    fun getAll(): Call<ArrayList<WasdappEntry>>

    @DELETE("get/{id}")
    fun deleteById(@Path("id") id: Long): Call<Boolean>

}