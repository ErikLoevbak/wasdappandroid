package com.realdolmen.wasdappgroepb.wasdapp.activities

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.google.zxing.integration.android.IntentIntegrator
import com.google.zxing.integration.android.IntentResult
import com.realdolmen.wasdappgroepb.wasdapp.R
import com.realdolmen.wasdappgroepb.wasdapp.adapters.RetroFitFactory.Companion.retrofit
import com.realdolmen.wasdappgroepb.wasdapp.adapters.WasdappAdapter
import com.realdolmen.wasdappgroepb.wasdapp.dataclasses.WasdappEntry
import com.realdolmen.wasdappgroepb.wasdapp.repo.WasdappRepo
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Homepage : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    private var layoutManager: RecyclerView.LayoutManager? = null
    private var wasDapps: ArrayList<WasdappEntry>? = null
    var scannedResult: String = ""
    private var adapter: WasdappAdapter? = null

    private lateinit var wasdappRepo: WasdappRepo
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initRepo()
        setDrawer()
        findAll()

    }


    private fun initRepo() {
        wasdappRepo = retrofit.create(WasdappRepo::class.java)
    }

    private fun findAll() {
        var findAllCall = wasdappRepo.getAll()
        setRecyclerView(findAllCall)
    }

    private fun filterByName(string: String) {
        var searchByNameCall = wasdappRepo.search(string)
        setRecyclerView(searchByNameCall)
    }

    private fun setRecyclerView(call: Call<ArrayList<WasdappEntry>>) {
        call.enqueue(object : Callback<ArrayList<WasdappEntry>> {
            override fun onResponse(call: Call<ArrayList<WasdappEntry>>, response: Response<ArrayList<WasdappEntry>>) {
                wasDapps = response.body()!!
                adapter = WasdappAdapter(wasDapps!!, this@Homepage, Activity = this@Homepage)
                layoutManager = LinearLayoutManager(this@Homepage)
                recyclerWasAppProduct.layoutManager = layoutManager
                recyclerWasAppProduct.adapter = adapter



                adapter!!.notifyDataSetChanged()
                loadingPanel.visibility = View.GONE

            }

            override fun onFailure(call: Call<ArrayList<WasdappEntry>>, t: Throwable) {

            }
        })

    }

    private fun setDrawer() {
        setSupportActionBar(toolbar)
        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        nav_view.setNavigationItemSelectedListener(this)
    }

    private var back_pressed: Long = 0
    private var toast: Toast? = null
    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {

            if (back_pressed + 2000 > System.currentTimeMillis()) {

                // need to cancel the toast here
                toast!!.cancel()

                // code for exit
                val intent = Intent(Intent.ACTION_MAIN)
                intent.addCategory(Intent.CATEGORY_HOME)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)

            } else {
                // ask user to press back button one more time to close app
                toast = Toast.makeText(baseContext, "Press BACK again to exit", Toast.LENGTH_SHORT)
                toast!!.show()
            }
            back_pressed = System.currentTimeMillis()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        searchFilter(menu)
        return true
    }

    private fun searchFilter(menu: Menu) {
        menuInflater.inflate(R.menu.main, menu)
        var searchView: SearchView = menu.findItem(R.id.search_entries).actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText.isNullOrBlank()) {
                    findAll()
                } else {
                    filterByName(newText)
                }
                return true
            }

        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_addUser -> {
                // Handle the camera action
                val intent = Intent(this, AddUser::class.java)
                intent.action = Intent.ACTION_SEND
                startActivity(intent)
            }
            R.id.nav_addRecord -> {
                val intent = Intent(this, Insert::class.java)
                startActivity(intent)
            }
            R.id.nav_scanCode -> {
                IntentIntegrator(this@Homepage).initiateScan()
            }
            R.id.nav_viewRecord -> {
                val intent = Intent(this, Homepage::class.java)
                intent.action = Intent.ACTION_SEND
                startActivity(intent)
            }
            R.id.nav_logOut -> {
                val intent = Intent(this, Login::class.java)
                intent.action = Intent.ACTION_SEND
                startActivity(intent)
            }
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    //Begin QR result scan
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        var result: IntentResult? = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)

        if (result != null && resultCode == RESULT_OK) {

            scannedResult = result.contents
            if (scannedResult.toLongOrNull() != null) {
                val intent = Intent(this, WasdappDetailPage::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.putExtra("idEntry", scannedResult.toLong())
                this.startActivity(intent)
            } else {
                val saveAlert = AlertDialog.Builder(this)
                saveAlert.setTitle("Scan Error")
                saveAlert.setMessage("QR code did not contain a number!")
                saveAlert.setPositiveButton("OK") { dialogInterface: DialogInterface, i: Int ->
                    dialogInterface.dismiss()
                }
                saveAlert.show()
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }
// End QR result scan


}
