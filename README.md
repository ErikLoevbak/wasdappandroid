README WasDappGroupB

Configuratie server
--------------------
De applicatie draait op een hotspot.

Project openen
-------------------------
- Zorg ervoor dat je bent geconnecteerd op de hotspot die je gegeven wordt.

- Vooraleer je de APK kan installeren op je gsm, moet je ervoor zorgen dat  third-party apps toegestaan zijn op je toestel.
- Ga naar Menu > Instellingen > Security > and check Onbekende Bronnen(Unknown Sources) om toe te staan dat je gsm andere apps download dan die van de Google Play Store.
- Eens je een gedownloade APK op je pc hebt, verbind je je Android toestel aan de pc.
- Bij het inpluggen van je toestel, zal je de keuze hebben tussen het opladen van de gsm of het connecteren als een 'Media device'. Kies voor 'Media device'.
- Daarna zoek je op de pc naar de folder van de gsm(op Windows zal het staan in 'Mijn Computer' of 'Computer').
- Kopieer de APK file in een map naar keuze van het Android toestel.
- Nu kan je zoeken naar de locatie van het bestand in de "Mijn bestanden" folder van je toestel.
- Vind het APK-bestand en druk op installeren.

All functionaliteiten(17-29 mei 2019)
--------------------------------------
Er is gewerkt met een hamburger-menu om een gestructureerde weergave van alle pagina's weer te geven
en om de connecties tussen de pagina's vlot te laten verlopen. 

Login
------
Op deze pagina kan de gebruiker inloggen of zich registreren. Deze gegevens worden in de backend opgeslagen.

Register
---------
Op de registerpagina kan de gebruiker zich registreren met gebruikersnaam en wachtwoord. Bij het registreren van een gebruiker wordt er ook aan validatie gedaan. 
Er wordt gecheckt of alle velden ingevuld zijn, of de wachtwoorden overeenkomen en of het wachtwoord min. 6 karakters en max. 25 karakters bevat.

Homepage
---------
De gebruiker kan op de homepage allerlei bewerkingen uitvoeren. Zo kan hij/zij records verwijderen of bewerken, bij het verwijderen van een record krijgt de gebruiker 
een pop-up te zien voor bevestiging van de actie. Bij het bewerken van data worden de huidige gegevens weergegeven in het formulier en worden ze vervangen door nieuwe data. 
Verder kan de gebruiker ook nieuwe data toevoegen via een formulier. Als de gebruiker op een card klikt in de homepage, krijgt hij/zij de details van deze card 
op een andere pagina met bijbehorende QR-code te zien.

Detail pagina
-------------
Op de detail pagina, krijgt de gebruiker de details te zien over een bepaalde card die hij/zij heeft geselecteerd op de homepage. 
Via deze pagina kan de gebruiker controlern of hij/zij in de buurt is van de QR-code die op dat moment wordt getoond. Als dit het geval is, dan kan de gebruiker deze locatie 
delen via verschillende kanalen(messenger, Instagram,...)

Add user
------------
Enkel een gebruiker die admin is kan andere gebruikers toevoegen. Een gewone gebruiker heeft deze mogelijkheid niet.

Add record
------------
Via deze pagina heeft de gebruiker de mogelijkheid om een record toe te voegen via een formulier. Deze data wordt dan toegevoegd aan de databank en is zichtbaar op de homepagina.

Scan code
-----------
Via deze pagina kan de gebruiker een QR-code scannen. Als de QR-code gescand is, gaat hij naar de detailpagina met de extra informatie op basis van de ID van de QR-code.

Log out
-------
De gebruiker kan zich uitloggen.